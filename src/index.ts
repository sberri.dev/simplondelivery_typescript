
// fonction qui permet d'additionner les chiffres
function add(a: number, b: number): number {
    let result : number = a + b
        return result
 }

 console.log(add(456, 993));

 // La fonction qui permet de convertir les devises
 function convertCurrency(amountToConvert: number, startingCurrency: string, arrivalCurrency: string) {

    // Convertir amountToConvert à partir de l'euro vers des dollars canadiens et yens
    if (startingCurrency === "EUR") { 
        if (arrivalCurrency === "CAD") { 
            return amountToConvert * 1.5;
        } else if (arrivalCurrency === "JPY") {
            return amountToConvert * 130;
        }
    // Convertir amountToConvert à partir des dollars canadiens vers l'euro et yens
    } else if (startingCurrency === "CAD") { 
        if (arrivalCurrency === "EUR") { 
            return amountToConvert * 0.67;
        } else if (arrivalCurrency === "JPY") {
            return amountToConvert * 87;
        }
    // Convertir amountToConvert à partir des yens vers de l'euro et des dollars canadiens 
    } else if (startingCurrency === "JPY") { 
        if (arrivalCurrency === "EUR") { 
            return amountToConvert * 0.0077;
        } else if (arrivalCurrency === "CAD") {
            return amountToConvert * 0.0115;
        }
    //Si la devise renseignée est différente des 3 devises de cette fonction, on renvoi ce message ci-dessous
    } else { 
        return "La devise renseignée n'est pas correcte"
    }
}

console.log(convertCurrency(18, "EUR", "CAD"))
console.log(convertCurrency(565, "JPY", "EUR"))
console.log(convertCurrency(15, "CAD", "JPY"))
console.log(convertCurrency(623, "CAD", "EUR"))

//Calculateur de frais de livraison
function deliveryCostCalculator (weightPackage : number, /*dimensions : { length: number, width: number, weight: number },*/ countryOfDestination : string ) { 

    //Cette variable price permet d'indiquer les tarifs de livraison en fonction du poids et des pays 
    const price : object = { 
        france : { until1Kg: 10, between1And3Kg: 20, more3kg: 30, extra: 5},
        canada : { until1Kg: 15, between1And3Kg: 30, more3kg: 45, extra: 7.5 },
        japon : { until1Kg: 1000, between1And3Kg: 2000, more3kg: 3000, extra: 500 }
    }

    //Condition qui calcule les prix en fonction du poids du paquet et du pays
    if (weightPackage >= 1 && countryOfDestination === "France") { 
        return 10; 
    } else if (weightPackage >= 3 && countryOfDestination === "France") { 
        return 20;
    } else if (weightPackage < 3 && countryOfDestination === "France") { 
        return 30;
    } else if (weightPackage < 1 && countryOfDestination === "Canada") { 
        return 15;
    } else if (weightPackage >= 3 && countryOfDestination === "Canada") { 
        return 30; 
    } else if (weightPackage < 3 && countryOfDestination === "Canada") { 
        return 45; 
    } else if (weightPackage >= 1 && countryOfDestination === "Japon") {
        return 1000;
    } else if (weightPackage >= 3 && countryOfDestination === "Japon") { 
        return 2000;
    } else if (weightPackage < 3 && countryOfDestination === "Japon") { 
        return 3000;
    } else { 
        return "renseignez le poids en kg"
    }  
}

console.log(deliveryCostCalculator(3, "France"))
console.log(deliveryCostCalculator(1, "Japon"))
console.log(deliveryCostCalculator(5, "Japon"))